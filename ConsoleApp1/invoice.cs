﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
class invoice
    {
         public string id;
         public string desc;
         public int qty;
         public double unitPrice;

        public invoice()
        {
            id = "item";
            desc = "Item description";
            qty = 1;
            unitPrice = 2;
        }
        public string getID()
        {
            Console.Write("Item ID: ");
            id = Console.ReadLine();
            return id;
        }
        public string getDesc()
        {
            Console.Write("Item description ");
            desc = Console.ReadLine();
            return desc;
        }
        public int getQty()
        {
            return qty;
        }
        public void setQty(int newQty)
        {
            qty = newQty;
        }
        public double getUnitPrice()
        {
            return unitPrice;
        }
        public void setUnitPrice(double newUnitPrice)
        {
            unitPrice = newUnitPrice;
        }
        public double getTotal()
        {
            return unitPrice * qty;
        }
        public override string ToString()
        {
            return "Invoice Item id: " + id + " description: " + desc + " quantity: " + qty + " unitprice: " + unitPrice + "Total:" + getTotal();
        }
    }
}
