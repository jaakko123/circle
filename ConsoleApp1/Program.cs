﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("1.1 testi osa");
            Circle c1 = new Circle(1.2, "pink");
            Console.WriteLine(c1);

            Console.WriteLine("\n");
            Console.WriteLine("1.2 Simplified Circle");
            SimplifiedCircle c2 = new SimplifiedCircle(2.5);
            Console.WriteLine(c2);

            Console.WriteLine("\n");
            Console.WriteLine("1.3 Rectangle");
            rectangle c3 = new rectangle();
            Console.WriteLine(c3.ToString());
            rectangle c3b = new rectangle();
            c3b.setWidth(2);
            c3b.setLength(4);
            Console.WriteLine(c3b.ToString());

            Console.WriteLine("\n");
            Console.WriteLine("1.4 employee");
            employee c4 = new employee();
            c4.FirstName();
            c4.LastName();
            Console.WriteLine();
            Console.WriteLine(c4.ToString());
            Console.WriteLine();
            employee c5 = new employee();
            c5.FirstName();
            c5.LastName();
            c5.setSalary(4000);
            c5.raiseSalary(500);
             Console.WriteLine();
            Console.WriteLine(c5.ToString());

            Console.WriteLine("\n");
            Console.WriteLine("1.5 invoice");
            invoice c6 = new invoice();
            c6.getID();
            c6.getDesc();
            Console.WriteLine(c6.ToString());
            Console.WriteLine();
            invoice c7 = new invoice();
            c7.getID();
            c7.getDesc();
            c7.setQty(5);
            c7.setUnitPrice(15);
            Console.WriteLine(c7.ToString());

            Console.WriteLine("\n");
            Console.WriteLine("1.6 account");
            account c8 = new account();
            Console.WriteLine(c8.ToString());
            Console.WriteLine();
            account c9 = new account();
            c9.setBalance(1000);
            c9.balance = c9.credit(500);
            c9.debit(800);
            c9.transferTo(c9, 500);
            Console.WriteLine(c9.ToString());
 
            Console.WriteLine("\n");
            Console.WriteLine("1.7 date");
            date c10 = new date();
            c10.getDay();
            c10.getMonth();
            c10.getYear();
            c10.Init(c10);
            if (c10.day.Contains(c10.userDay) && c10.month.Contains(c10.userMonth) && c10.year.Contains(c10.userYear))
            {
                Console.WriteLine(c10.toString());
            }
            else
            {
                Console.WriteLine("Invalid date, month or year");
            }
            Console.WriteLine("\n");
            Console.WriteLine("1.8 time");
            time c11 = new time();
            Console.WriteLine(c11.toString());
            Console.WriteLine();
            time c12 = new time();
            c12.setHour(5);
            c12.setMinute(45);
            c12.setSecond(9);
            Console.WriteLine(c12.toString());




        }
    }
}
