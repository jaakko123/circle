﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
namespace ConsoleApp1
{
    internal class Circle
    {
        private double radius;
        private string color;

        // The default constructor with no argument.
        // It sets the radius and color to their default value.
        public Circle()
        {
            this.radius = 1;
            this.color = "red";
        }

        // 2nd constructor with given radius, but color default
        public Circle(double radius)
        {
            this.radius = radius;
            color = "red";
        }
        public Circle(double radius, string color)
        {
            this.radius = radius;
            this.color = color;
        }
      
        // A public method for retrieving the radius
        public double getRadius()
        {
            return radius;
        }

        // A public method for computing the area of circle
    
        public string getColor()
        {
            return color;
        }

        public double setRadius(double radius)
        {
         return this.radius = radius;
        }

        public void setColor(String Color)
        {
            this.color = color;
        }
        public override string ToString()
        {
            return "Circle radius=" + radius + " color=" + color;
        }
        public double getArea()
        {
            return radius * radius * System.Math.PI;
        }
    }

}