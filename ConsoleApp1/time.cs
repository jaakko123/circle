﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    namespace ConsoleApp1
    {
        class time
        {
              public int hour, minute, second;
              public time()
                {
                    hour = 1;
                    minute = 1;
                    second = 1;
                }
              public int getHour()
                {
                    return hour;
                }
              public int getMinute()
                {
                    return minute;
                }
              public int getSecond()
                {
                return second;
                }

              public void setHour(int newHour)
                 {
                hour = newHour;
                }

              public void setMinute(int newMinute)
                {
                minute = newMinute;
                }

              public void setSecond(int newSecond)
             {
                second = newSecond;
                }

              public void setTime(int _hour, int _minute, int _second)
                {
                hour = _hour;
                minute = _minute;
                second = _second;
                }

              public string toString()
                {
                return String.Format("{0:00}:{1:00}:{2:00}", hour, minute, second);
                }

        }
    }
