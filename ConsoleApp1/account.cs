﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class account
    {
        public string id;
        public string name;
        public int balance = 0;

        public account()
        {
            id = "ID";
            name = "Name";
        }

        public void Account2(string id2, string name2, int balance2)
        {
            id = id2;
            name = name2;
            balance = balance2;
        }

        public string getID()
        {
            return id;
        }

        public string getName()
        {
            return name;
        }

        public int getBalance()
        {
            return balance;
        }
        public void setBalance(int newBalance)
        {
            balance = newBalance;
        }
        public int credit(int amount)
        {
            return amount + balance;
        }

        public int debit(int amount)
        {
            if (amount <= balance)
            {
                balance = balance - amount;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
            }

            return balance;
        }

        public int transferTo(account another, int amount)
        {
            if (amount <= balance)
            {
                another.balance += amount;
            }
            else
            {
                Console.WriteLine("Amount exceeded balance");
            }

            return another.balance;
        }

        public override string ToString()
        {
            return "Account id: " + id + "\n" + "Name: " + name + "\n" + "Balance: " + balance;
        }

    }
}
