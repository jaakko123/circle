﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class employee
    {

        int id = 1;
        string firstName, lastName;
        int salary = 2000;
        int annualSalary;


        public int getID()
        {
            return id;
        }
        public string FirstName()
        {
            Console.Write("First name:");
            firstName = Console.ReadLine();
            return firstName;
        }
        public string LastName()
        {
            Console.Write("Last name: ");
            lastName = Console.ReadLine();
            return lastName;
        }
        public string Name()
        {
            return firstName +" "+ lastName;
        }
        public int getSalary()
        {
            return salary;
        }
        public void setSalary(int newSalary)
        {
            salary = newSalary;
        }
        public int AnnualSalary()
        {
            annualSalary = salary * 12;
            return annualSalary;
        }
        public int raiseSalary(int percent)
        {
            salary = salary + percent;
            return salary;
        }
        public override string ToString()
        {
            return "id: " + getID() + "\n" + Name() + " salary: " + getSalary() + "\n" + "annual salary: " + AnnualSalary();
        }

    }
}
