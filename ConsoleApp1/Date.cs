﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class date
    {
        public int myDay, myYear, myMonth;
        public List<int> day = new List<int>();
        public List<int> month = new List<int>();
        public List<int> year = new List<int>();
        public int userDay;
        public int userMonth;
        public int userYear;

        public date(int day2, int month2, int year2)
        {
            myDay = day2;
            myMonth = month2;
            myYear = year2;
        }

        public date()
        {
        }

        public int getDay()
        {
            Console.Write("Day: ");
            string s;
            s = Console.ReadLine();
            int.TryParse(s, out userDay);
            return userDay;
        }
        public int getMonth()
        {
            Console.Write("Month: ");
            string s;
            s = Console.ReadLine();
            int.TryParse(s, out userMonth);
            return userMonth;
        }

        public int getYear()
        {
            Console.Write("Year: ");
            string s;
            s = Console.ReadLine();
            int.TryParse(s, out userYear);
            return userYear;
        }
        public void setDay(int newDay)
        {
            myDay = newDay;
        }

        public void setMonth(int newMonth)
        {
            myMonth = newMonth;
        }

        public void setYear(int newYear)
        {
            myYear = newYear;
        }

        public void setDate(int day, int month, int year)
        {
            myDay = day;
            myMonth = month;
            myYear = year;
        }

        public string toString()
        {
            return userDay + "/" + userMonth + "/" + userYear.ToString();
        }

        public void Init(date d)
        {
            for (int i = 1; i <= 31; i++)
            {
                d.day.Add(i);
            }
            for (int i = 1; i <= 12; i++)
            {
                d.month.Add(i);
            }
            for (int i = 1900; i <= 9999; i++)
            {
                d.year.Add(i);
            }
        }
    }
}

        
    
