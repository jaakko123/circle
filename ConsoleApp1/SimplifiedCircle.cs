﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class SimplifiedCircle
    {
        private double radius;

        public SimplifiedCircle()
        {
            radius = 1.0;
        }
        public SimplifiedCircle(double radius)
        {
            this.radius = radius;
        }
        public double getRadius(double radius)
        {
            return radius;
        }
        public void setRadius(double radius)
        {
            this.radius = radius;
        }
        public double getArea()
        {
            return radius * radius * Math.PI;
        }
        public double getCircumference()
        {
            return radius * Math.PI;
        }
        public override String ToString()
        {
            return "SImplified circle radius=" + radius + " area=" + getArea() + " circumference=" + getCircumference();
        }
    }
}
